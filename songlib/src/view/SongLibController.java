//Surekha Gurung
//Diamond Pham

package view;

import javafx.fxml.FXML;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.stage.Stage;

public class SongLibController {

    @FXML Button add;
    @FXML Button delete;
    @FXML Button edit;
    @FXML Button SaveChanges;
    @FXML TextField addSongTitle;
    @FXML TextField addArtistName;
    @FXML TextField addAlbumName;
    @FXML TextField addAlbumYear;
    @FXML TextField songTitleTxt;
    @FXML TextField songArtistTxt;
    @FXML TextField songAlbumTxt;
    @FXML TextField songYearTxt;

    @FXML
    public ListView<String> songList = new ListView<String>();

    	public Song song;

    	ArrayList<Song> songArrayList = new ArrayList<Song>();
    	ArrayList<String> songTitlesList = new ArrayList<String>();
    	List<Song> songArtistList = new ArrayList<Song>();

    	ObservableList<String> obsList;

    public void start(Stage mainStage) {
    	
    	try
    	{
    		FileReader reader = new FileReader("songs.txt");
    		BufferedReader bReader = new BufferedReader(reader);
    		
    		String line;
    		
    		int index = 0; 
    		while((line = bReader.readLine()) != null)
    		{
    			Song temp = new Song("temp", "temp", null, null);
    			temp.setSong_Title(line);
    			line = bReader.readLine();
    			temp.setArtist_name(line);
    			line = bReader.readLine();
    			temp.setAlbum_name(line);
    			line = bReader.readLine();
    			temp.setYear(line);
    			songArrayList.add(index, temp);
    			index++;
    		}
    		reader.close();
    		
    		int i;
            for(i = 0; i < songArrayList.size(); i++)
            {
                songTitlesList.add(i, songArrayList.get(i).getTitle());
                
            }
    	}
    	catch (IOException e)
    	{
    		e.printStackTrace();
    	}

    	obsList = FXCollections.observableArrayList(songTitlesList);
    	songList.setItems(obsList);
    	
    	addSongTitle.setFocusTraversable(false);
        addArtistName.setFocusTraversable(false);
        addAlbumName.setFocusTraversable(false);
        addAlbumYear.setFocusTraversable(false);
        songTitleTxt.setFocusTraversable(false);
        songArtistTxt.setFocusTraversable(false);
        songAlbumTxt.setFocusTraversable(false);
        songYearTxt.setFocusTraversable(false);
        add.setFocusTraversable(false);
        delete.setFocusTraversable(false);
        edit.setFocusTraversable(false);

    	//select the first item
    	songList.getSelectionModel().select(0);

    	songList
    		.getSelectionModel()
    		.selectedItemProperty()
    		.addListener( (obs, oldVal, newVal)->displaySongInfo(mainStage));
			 
    }

    //add button clicked
    @FXML
    private void addSong(ActionEvent event) throws IOException {
    	Song newSong = new Song(addSongTitle.getText(), addArtistName.getText(),
    							addAlbumName.getText(), addAlbumYear.getText() );
    	int i;
    	//user doesn't provide song title or artist; invalid
    	if( newSong.getArtist().equals("") || newSong.getTitle().equals("")) {
    		Alert alert = new Alert(AlertType.ERROR);
    		alert.setTitle("Error Dialog");
    		alert.setContentText("Ooops, please at least write song title and artist name to be added");
    		alert.showAndWait();
    		throw new IllegalArgumentException();
    	}
    	
    	//song already exists
    	for(i=0; i<songTitlesList.size(); i++) {
    		if( (newSong.getTitle().equalsIgnoreCase(songTitlesList.get(i))) && 
    			(newSong.getArtist().equalsIgnoreCase(songArtistList.get(i).getArtist()))    ) {
    			
    				System.out.println("same song index: "+i);
    				System.out.println("same song in titles list: " +songTitlesList.get(i));
    				System.out.println("same artist in artist list: " +songArtistList.get(i));
    				
    				Alert alert = new Alert(AlertType.ERROR);
    				alert.setTitle("Error Dialog");
    				alert.setContentText("Oops, song already exists");
    				alert.showAndWait();
    				throw new IllegalArgumentException();
    		}
    	}
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Confirmation");
        alert.setContentText("Do you want to add this song?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK)
        {	
            songArrayList.add(newSong);
            Collections.sort(songArrayList);
            
            songTitlesList.add(newSong.getTitle());
            Collections.sort(songTitlesList, String.CASE_INSENSITIVE_ORDER);
            
            songArtistList.add(newSong);
            Collections.sort(songArtistList, new ArtistsortByTitleComparator());
            Collections.sort(songArtistList, new Comparator<Song>() {
            	@Override
            	public int compare( Song o1, Song o2) {
            		return o1.getTitle().toLowerCase().compareTo(o2.getTitle().toLowerCase());
            	}
            });
            
            //clear text fields after button is clicked
            addSongTitle.setText("");
            addArtistName.setText("");
            addAlbumName.setText("");
            addAlbumYear.setText("");

            obsList = FXCollections.observableArrayList(songTitlesList);
            songList.setItems(obsList);
            
            int songIndex = songArrayList.indexOf(newSong);
            songList.getSelectionModel().select(songIndex);
            
            System.out.println("YOO OVER HERE "+songIndex);
            
            PrintWriter w2 = new PrintWriter("songs.txt");
            w2.print("");
            w2.close();
            FileWriter writer = new FileWriter("songs.txt", true);
            int index;
            try
            {
            	for(index = 0; index < songArrayList.size(); index++)
            	{
            		writer.write(songArrayList.get(index).getTitle());
            		writer.write("\r\n");
            		writer.write(songArrayList.get(index).getArtist());
            		writer.write("\r\n");
            		writer.write(songArrayList.get(index).getAlbum());
            		writer.write("\r\n");
            		writer.write(songArrayList.get(index).getYear());
            		writer.write("\r\n");
  
            	}
            	writer.close();
            }
            catch (IOException e)
            {
            	e.printStackTrace();
            } 
            
        }
        else
        {
            addSongTitle.setText("");
            addArtistName.setText("");
            addAlbumName.setText("");
            addAlbumYear.setText("");
        }
        for(i=0; i<songArtistList.size(); i++) {
        	System.out.println("index:"+i+" songArtistList: "+songArtistList.get(i).getArtist());
        }
        for(i=0; i<songTitlesList.size(); i++) {
        	System.out.println("songTitlesList: "+songTitlesList.get(i));
        }
        for(i=0; i<songArrayList.size(); i++) {
        	System.out.println("song title/artist in array list : "+songArrayList.get(i).getTitle() 
        						+ songArrayList.get(i).getArtist());
        }
        
    }

    //delete button clicked
    @FXML
    private void deleteSong(ActionEvent event) throws IOException {
    	
    	int deleteIndex = songList.getSelectionModel().getSelectedIndex();
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Confirmation");
        alert.setContentText("Do you want to delete this song?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK)
        {
        	
        	if( deleteIndex>=0) {
    		
        		if( deleteIndex==0 && songArrayList.size()==1){
        			songArrayList.remove(deleteIndex);
        			songTitlesList.remove(deleteIndex);
        			obsList = FXCollections.observableArrayList(songTitlesList);
        			songList.setItems(obsList);
        			songTitleTxt.setText("");
        			songArtistTxt.setText("");
        			songAlbumTxt.setText("");
        			songYearTxt.setText("");
        			
        			PrintWriter w2 = new PrintWriter("songs.txt");
                    w2.print("");
                    w2.close();
                    FileWriter writer = new FileWriter("songs.txt", true);
                    int index;
                    try
                    {
                    	for(index = 0; index < songArrayList.size(); index++)
                    	{
                    		writer.write(songArrayList.get(index).getTitle());
                    		writer.write("\r\n");
                    		writer.write(songArrayList.get(index).getArtist());
                    		writer.write("\r\n");
                    		writer.write(songArrayList.get(index).getAlbum());
                    		writer.write("\r\n");
                    		writer.write(songArrayList.get(index).getYear());
                    		writer.write("\r\n");
          
                    	}
                    	writer.close();
                    }
                    catch (IOException e)
                    {
                    	e.printStackTrace();
                    }
        		}
        		else if(deleteIndex==songArrayList.size()-1){
        			songArrayList.remove(deleteIndex);
        			songTitlesList.remove(deleteIndex);
        			obsList = FXCollections.observableArrayList(songTitlesList);
        			songList.setItems(obsList);
        			songList.getSelectionModel().select(deleteIndex-1);
        			
        			PrintWriter w2 = new PrintWriter("songs.txt");
                    w2.print("");
                    w2.close();
                    FileWriter writer = new FileWriter("songs.txt", true);
                    int index;
                    try
                    {
                    	for(index = 0; index < songArrayList.size(); index++)
                    	{
                    		writer.write(songArrayList.get(index).getTitle());
                    		writer.write("\r\n");
                    		writer.write(songArrayList.get(index).getArtist());
                    		writer.write("\r\n");
                    		writer.write(songArrayList.get(index).getAlbum());
                    		writer.write("\r\n");
                    		writer.write(songArrayList.get(index).getYear());
                    		writer.write("\r\n");
          
                    	}
                    	writer.close();
                    }
                    catch (IOException e)
                    {
                    	e.printStackTrace();
                    }
        		} else {
        			songArrayList.remove(deleteIndex);
        			songTitlesList.remove(deleteIndex);
        			obsList = FXCollections.observableArrayList(songTitlesList);
        			songList.setItems(obsList);
        			songList.getSelectionModel().select(deleteIndex);
        			
        			PrintWriter w2 = new PrintWriter("songs.txt");
                    w2.print("");
                    w2.close();
                    FileWriter writer = new FileWriter("songs.txt", true);
                    int index;
                    try
                    {
                    	for(index = 0; index < songArrayList.size(); index++)
                    	{
                    		writer.write(songArrayList.get(index).getTitle());
                    		writer.write("\r\n");
                    		writer.write(songArrayList.get(index).getArtist());
                    		writer.write("\r\n");
                    		writer.write(songArrayList.get(index).getAlbum());
                    		writer.write("\r\n");
                    		writer.write(songArrayList.get(index).getYear());
                    		writer.write("\r\n");
          
                    	}
                    	writer.close();
                    }
                    catch (IOException e)
                    {
                    	e.printStackTrace();
                    }
        		}

        	} else {
        		//Nothing selected
        		Alert alert2 = new Alert(AlertType.WARNING);
        		alert2.setTitle("No Selection");
        		alert2.setHeaderText("No Song Selected");
        		alert2.setContentText("Please select a song to delete.");
        		alert2.showAndWait();
        	}
        } 
        else 
        {
        	obsList = FXCollections.observableArrayList(songTitlesList);
            songList.setItems(obsList);
        
        }
    }
   
    //displays any selected song information
    private void displaySongInfo(Stage mainStage) {
    	
    	int displayIndex = songList.getSelectionModel().getSelectedIndex();
    	System.out.println("displayIndex: "+displayIndex);
    	
    	Song selectedSong = songArrayList.get(displayIndex);
    	songTitleTxt.setText(selectedSong.getTitle());
    	songArtistTxt.setText(selectedSong.getArtist());
    	songAlbumTxt.setText(selectedSong.getAlbum());
    	songYearTxt.setText(selectedSong.getYear());
    	
    }
    
    //displays edited song's info on mainStage
    private void showSongDetails(Song song) {
    	System.out.println("do i get used?");
    	if(song!=null) {
    		songTitleTxt.setText(song.getTitle());
    		songArtistTxt.setText(song.getArtist());
    		songAlbumTxt.setText(song.getAlbum());
    		songYearTxt.setText(song.getYear());
        	
    	} else {
    		songTitleTxt.setText("");
    		songArtistTxt.setText("");
    		songAlbumTxt.setText("");
    		songYearTxt.setText("");
    	}
    	int songIndex = songArrayList.indexOf(song);
        songList.getSelectionModel().select(songIndex);
    } 
     
    //adds the editedSong title to list for user to see
    private void addtoList(int index, Song selectedSong) {
    	int i;
    	int x=0, y=0;
    	if(selectedSong.getAlbum()==null || selectedSong.getAlbum().length()==0 ) {  x++; } else {y++;}
    	if(selectedSong.getYear()==null || selectedSong.getYear().length()==0 ) {  x++; } else {y++;}
    	
    	System.out.println("y = "+y);
    	System.out.println("x = " +x);
    	
    	for(i=0; i<songTitlesList.size(); i++) {
    		if( (selectedSong.getTitle().equalsIgnoreCase(songTitlesList.get(i))) && 
    			(selectedSong.getArtist().equalsIgnoreCase(songArtistList.get(i).getArtist()))  /* && y==2 */ ) {
    			
    				System.out.println("same song index: "+i);
    				System.out.println("same song in titles list: " +songTitlesList.get(i));
    				System.out.println("same artist in artist list: " +songArtistList.get(i));
    				
    				Alert alert = new Alert(AlertType.ERROR);
    				alert.setTitle("Error Dialog");
    				alert.setContentText("Oops, song already exists");
    				alert.showAndWait();
    				throw new IllegalArgumentException();
    		}
    	}
    	
		songArrayList.set(index, selectedSong);
		System.out.println("selected song in here: "+selectedSong.getTitle());
		Collections.sort(songArrayList);
		
		songTitlesList.set(index, selectedSong.getTitle());
		Collections.sort(songTitlesList, String.CASE_INSENSITIVE_ORDER); 
		songArtistList.add(selectedSong);
        Collections.sort(songArtistList, new ArtistsortByTitleComparator());
        Collections.sort(songArtistList, new Comparator<Song>() {
        	@Override
        	public int compare( Song o1, Song o2) {
        		return o1.getTitle().compareTo(o2.getTitle());
        	}
        });
		
		obsList = FXCollections.observableArrayList(songTitlesList);
    	songList.setItems(obsList);
        songList.getSelectionModel().select(index);
        
    }
    
    private boolean inputValid(Song song) {
    	int x =0, y=0;
    	if(addSongTitle.getText()==null || addSongTitle.getText().length()==0 ) { x++; }
    	if(addArtistName.getText()==null || addArtistName.getText().length()==0 ) {  x++;  }
    	if(addAlbumName.getText()==null || addAlbumName.getText().length()==0 ) {  x++; } else {y++;}
    	if(addAlbumYear.getText()==null || addAlbumYear.getText().length()==0 ) {  x++; } else {y++;}
    	
    	System.out.println("y = "+y);
    	System.out.println("x = " +x);
    	if(x==4){
    		//error message
    		Alert alert = new Alert(AlertType.ERROR);
    		alert.setTitle("Invalid Fields");
    		alert.setHeaderText("Please correct invalid fields");
    		alert.setContentText("You didn't enter any changes");
    		alert.showAndWait();
    		return false;
    	} 
   
    	//user doesn't provide song title and artist name
    	if( (addSongTitle.getText()==null || addSongTitle.getText().length()==0) || 
    		(addArtistName.getText()==null || addArtistName.getText().length()==0) ) {
        		//error message
        		Alert alert = new Alert(AlertType.ERROR);
        		alert.setTitle("Invalid Fields");
       			alert.setHeaderText("Please correct invalid fields");
       			alert.setContentText("Please provide a song title and artist name to be added");
       			alert.showAndWait();
       			return false;   
    	}
    	
   		int i;
   		for(i=0; i<songTitlesList.size(); i++) {
   			if( (addSongTitle.getText().equalsIgnoreCase(songTitlesList.get(i))) && 
 				(addArtistName.getText().equalsIgnoreCase(songArtistList.get(i).getArtist()))  && y==2  ) {
   					System.out.println("I'm here");
   					Alert alert2 = new Alert(AlertType.ERROR);
   					alert2.setTitle("Error Dialog");    
   					alert2.setHeaderText("Can not add song");
   					alert2.setContentText("Oops, song already exists");
  					alert2.showAndWait();
 					return false;
    		} 
    	} 
        return true;
    
    }
    
    public void setSong(Song song) {
    	this.song = song;
    	
    	addSongTitle.setText(song.getTitle());
    	System.out.println("song.getTitle(): "+song.getTitle());
    	addArtistName.setText(song.getArtist());
    	addAlbumName.setText(song.getAlbum());
    	addAlbumYear.setText(song.getYear());
    	
    } 

    
    //edit button clicked
    @FXML
    private void editSong(ActionEvent event) {
    	
    	System.out.println("at least this works");

    	int editIndex = songList.getSelectionModel().getSelectedIndex();
    	System.out.println("editIndex: "+editIndex);
    	Song selectedSong = songArrayList.get(editIndex);
    	System.out.println("selectedSong: "+selectedSong);
    	
    	setSong(selectedSong);
    	
    	
    }
    
    @FXML
    private void clickedSaveChanges(ActionEvent event) {
        
    	int editIndex = songList.getSelectionModel().getSelectedIndex();
    	System.out.println("editIndex: "+editIndex);
    	Song selectedSong = songArrayList.get(editIndex);
    	System.out.println("selectedSong: "+selectedSong);
    	
    	if( inputValid(selectedSong) ) {
    		selectedSong.setSong_Title(addSongTitle.getText());
    		System.out.println("new song title: "+song.getTitle());
    		selectedSong.setArtist_name(addArtistName.getText());
    		selectedSong.setAlbum_name(addAlbumName.getText());
    		selectedSong.setYear(addAlbumYear.getText());
    		showSongDetails(selectedSong);
    		addtoList(editIndex, selectedSong);
    		
    	}
    	//clear text fields after button is clicked
        addSongTitle.setText("");
        addArtistName.setText("");
        addAlbumName.setText("");
        addAlbumYear.setText("");
    }
}

















