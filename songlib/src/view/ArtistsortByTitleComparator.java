//Surekha and Diamond

package view;

import java.util.Comparator;

public class ArtistsortByTitleComparator implements Comparator<Song>{
	
	@Override
	public int compare(Song o1, Song o2) {
		
		int value = o1.getTitle().toLowerCase().compareTo(o2.getTitle().toLowerCase());
		if(value>0 || value<0) {
			return value;
		} else {
			value = o1.getArtist().toLowerCase().compareTo(o2.getArtist().toLowerCase());
			System.out.println("o1.getArtist(): "+o1.getArtist());
			System.out.println("o2.getArtist(): "+o2.getArtist());
			System.out.println("value: "+value);
			return value;
		}
	}

}
