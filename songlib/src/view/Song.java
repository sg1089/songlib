//Surekha Gurung 
//Diamond Pham

package view;

public class Song implements Comparable<Song>{ 
	
	private String song_title;
	private String artist_name;
	private String album_name;
	private String album_year;
	
	//constructor
	public Song (String song_title, String artist_name, String album_name, String album_year ) {
		
		if(song_title==null || artist_name==null) {
			throw new NullPointerException();
		}
		this.song_title = song_title;
		this.artist_name = artist_name;
		this.album_name = album_name;
		this.album_year = album_year;
		
	}
	
	public String getTitle() {
		return this.song_title;
	}
	
	public String getArtist() {
		return this.artist_name;
	}

	public String getAlbum() {
		return this.album_name;
	}
	
	public String getYear() {
		return this.album_year;
	}
	
	public void setSong_Title(String title){
		this.song_title = title;
	}
	
	public void setArtist_name(String name) {
		this.artist_name = name;
	}
	
	public void setAlbum_name(String album) {
		this.album_name = album;
	}
	
	public void setYear(String year) {
		this.album_year = year;
	}

	@Override
	public int compareTo(Song newSong) throws ClassCastException {
		// TODO Auto-generated method stub
		if( !(newSong instanceof Song) ) {
			throw new ClassCastException("Not a song object.");
		}
		
		int value = this.song_title.toLowerCase().compareTo(newSong.getTitle().toLowerCase());
		if(value>0 || value<0) {
			return value;
		} else {
			value = this.artist_name.toLowerCase().compareTo(newSong.getArtist().toLowerCase());
			System.out.println("this.artist_name: "+this.artist_name);
			System.out.println("newSong.artist_name: "+newSong.getArtist());
			return value;
		}
		
		/* //String songTitle = this.song_title;
		return this.song_title.toLowerCase().compareTo(newSong.getTitle().toLowerCase()); */
	}
	

}
