//Surekha Gurung
//Diamond Pham

package application;

import javafx.application.Application;


import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import view.SongLibController;


public class SongLib extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("SongLib.fxml"));

			AnchorPane root = (AnchorPane)loader.load();

			SongLibController songLibController = loader.getController();
			songLibController.start(primaryStage);

			Scene scene1 = new Scene(root,478,455);
			primaryStage.setScene(scene1);
			primaryStage.setTitle("Song Library");
			primaryStage.setResizable(false);
			primaryStage.show();
			
	} 
	
	public static void main(String[] args) {
		launch(args);
	}

}
